<?php

namespace LaraDev;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use LaraDev\Support\Cropper;

class PropertyImage extends Model
{
    protected $fillable = [
        'property',
        'path',
        'cover'
    ];

    public function getUrlCroppedAttribute($key = null)
    {
        return Storage::url(Cropper::thumb($this->path, 1336, 768));
    }
}

<?php

namespace LaraDev\Http\Controllers\Admin;

use Illuminate\Http\Request;
use LaraDev\Contract;
use LaraDev\Http\Controllers\Controller;
use LaraDev\Http\Requests\Admin\Contract as ContractRequest;
use LaraDev\Property;
use LaraDev\User;

class ContractController extends Controller
{

    public function index()
    {
        $contracts = Contract::with(['ownerObject', 'acquirerObject'])->orderBy('id', 'DESC')->get();


        return view('admin.contracts.index', [
            'contracts' => $contracts
        ]);
    }


    public function create()
    {
        $lessors = User::lessors();
        $lessees = User::lessees();
        return view('admin.contracts.create', [
            'lessors' => $lessors,
            'lessees' => $lessees
        ]);
    }


    public function store(ContractRequest $request)
    {
        // $contract = new Contract();
        // $contract->fill($request->all());
        // var_dump($contract->getAttributes());

        $contractCreate =  Contract::create($request->all());
        var_dump($contractCreate);

        return redirect()->route('admin.contracts.edit', [
            'contract' => $contractCreate->id
        ])->with(['color' => 'green', 'message' => 'Contrato Cadastrado com Sucesso!']);
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $contract = Contract::where('id', $id)->first();
        $lessors = User::lessors();
        $lessees = User::lessees();

        return view('admin.contracts.edit', [
            'contract' => $contract,
            'lessors' => $lessors,
            'lessees' => $lessees
        ]);
    }

    public function update(ContractRequest $request, $id)
    {
        $contract = Contract::where('id', $id)->first();
        $contract->fill($request->all());
        $contract->save();

        if($request->property){
            $property = Property::where('id', $request->property)->firts();
            if($request->status === 'active'){
                $property->status = 0;
                $property->save();
            }else{
                $property->status = 1;
                $property->save();
            }
        }

        return redirect()->route('admin.contracts.edit', [
            'contract' => $contract->id
        ])->with(['color' => 'green', 'message' => 'Contrato editado com Sucesso!']);
    }

    public function destroy($id)
    {
        //
    }
    public function getDataOwner(Request $request)
    {
        $lessor = User::where('id', $request->user)->first([
            'id', 'civil_status', 'spouse_name', 'spouse_document'
        ]);

        if (empty($lessor)) {

            $spouse = null;
            $companies = null;
            $properties = null;
        } else {

            $civilStatusSpouseRequired = [
                'married',
                'separated'
            ];

            if (in_array($lessor->civil_status, $civilStatusSpouseRequired)) {
                $spouse = [
                    'spouse_name' => $lessor->spouse_name,
                    'spouse_document' => $lessor->spouse_document

                ];
            } else {
                $spouse = null;
            }
            $companies = $lessor->companies()->get([
                'id',
                'alias_name',
                'document_company'
            ]);

            $getProperties = $lessor->properties()->get();

            $property = [];

            foreach ($getProperties as $property) {
                $properties[] =  [
                    'id' => $property->id,
                    'description' => '#' . $property->id . ' ' . $property->street . ',' . $property->number .
                        ' ' .  $property->neighborhood . ' ' . ' ' . $property->city . '/' . $property->state
                        . '(' . $property->zipcode . ')'
                ];
            }
        }


        $json['spouse'] = $spouse;
        $json['companies'] =  (!empty($companies) &&  $companies->count() ? $companies : null);
        $json['properties'] = (!empty($properties) ? $properties : null);
        return response()->json($json);
    }

    public function getDataAcquirer(Request $request)
    {
        $lessee = User::where('id', $request->user)->first([
            'id', 'civil_status', 'spouse_name', 'spouse_document'
        ]);

        if (empty($lessee)) {
            $spouse = null;
            $companies = null;
        } else {

            $civilStatusSpouseRequired = [
                'married',
                'separated'
            ];

            if (in_array($lessee->civil_status, $civilStatusSpouseRequired)) {
                $spouse = [
                    'spouse_name' => $lessee->spouse_name,
                    'spouse_document' => $lessee->spouse_document

                ];
            } else {
                $spouse = null;
            }
            $companies = $lessee->companies()->get([
                'id',
                'alias_name',
                'document_company'
            ]);
        }

        $json['spouse'] = $spouse;
        $json['companies'] =  (!empty($companies) &&  $companies->count() ? $companies : null);
        return response()->json($json);
    }



    public function getDataProperty(Request $request)
    {
        $property  = Property::where('id', $request->property)->first();

        if (empty($property)) {
            $property = null;
        } else {
            $property = [
                'id' => $property->id,
                'sale_price' => $property->sale_price,
                'rent_price' => $property->rent_price,
                'tribute' => $property->tribute,
                'condominium' => $property->condominium
            ];
        }
        $json['property'] = $property;
        return response()->json($json);
    }
}

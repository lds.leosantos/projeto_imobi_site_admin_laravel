<?php

namespace LaraDev\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use LaraDev\Http\Controllers\Controller;
use LaraDev\Http\Requests\Admin\User as UserRequest;
use LaraDev\Support\Cropper;
use LaraDev\User;

class UserController extends Controller
{

    public function index()
    {
        $users = User::all();
        return  view('admin.users.index', [
            'users' => $users
        ]);
    }

    public function team()
    {
        $users = User::where('admin', 1)->get();
        return  view(
            'admin.users.team',
            [
                'users' => $users
            ]
        );
    }


    public function create()
    {
        return  view('admin.users.create');
    }


    public function store(UserRequest $request)
    {


        $userCreate = User::create($request->all());

        if (!empty($request->file('cover'))) {
            $userCreate->cover = $request->file('cover')->store('user');
            $userCreate->save();
        }

        return redirect()->route('admin.users.edit', [
            'users' => $userCreate->id
        ])->with(['color' => 'green', 'message' => 'cliente cadastrado com sucesso']);
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {

        $user = User::where('id', $id)->first();

        return view('admin.users.edit', [
            'user' => $user
        ]);
    }


    public function update(UserRequest $request, $id)
    {
        $user = User::where('id', $id)->first();

        $user->setLessorAttribute($request->lessor);
        $user->setLesseeAttribute($request->lessee);
        $user->setAdminAttribute($request->admin);
        $user->setClientAttribute($request->client);


        if (!empty($request->file('cover'))) {
            Storage::delete($user->cover);
            Cropper::flush($user->cover);
            $user->cover = '';
        }

        $user->fill($request->all());


        if (!empty($request->file('cover'))) {
            $user->cover = $request->file('cover')->store('user');
        }

        if (!$user->save()) {
            return redirect()->back()->withInput()->withErrors('');
        }

        return redirect()->route('admin.users.edit', [
            'users' => $user->id
        ])->with(['color' => 'green', 'message' => 'cliente atualizado com sucesso']);
    }


    public function destroy($id)
    {
        //teste
    }
}

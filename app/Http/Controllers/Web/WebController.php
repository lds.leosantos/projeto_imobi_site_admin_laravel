<?php

namespace LaraDev\Http\Controllers\Web;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use LaraDev\Http\Controllers\Controller;
use LaraDev\Mail\Web\Contact;
use LaraDev\Property;

class WebController extends Controller
{
    public function home()
    {


        $head = $this->seo->render(
            env('APP_NAME') . ' Upinside treinamentos',
            'Encontre o imoveis do seu sonho',
            route('web.home'),
            asset('frontend/assets/images/share.png')
        );
        $propertiesForSale = Property::sale()->available()->limit(3)->get();
        $propertiesForRent = Property::rent()->available()->limit(3)->get();

        return view('web.home', [
            'head' => $head,
            'propertiesForSale' => $propertiesForSale,
            'propertiesForRent' =>  $propertiesForRent
        ]);
    }

    public function spotlight()
    {
        return view('web.spotlight');
    }
    public function rent()
    {

        $head = $this->seo->render(
            env('APP_NAME') . ' Upinside treinamentos',
            'Alugue o imoveis do seu sonho',
            route('web.rent'),
            asset('frontend/assets/images/share.png')
        );

        $filter = new FilterController();
        $filter->clearAllData();

        $properties = Property::rent()->available()->get();


        return view('web.filter', [
            'head' => $head,
            'properties' => $properties,
            'type' => 'rent'
        ]);
    }

    public function buy()
    {

        $head = $this->seo->render(
            env('APP_NAME') . ' Upinside treinamentos',
            'Compre o imoveis do seu sonho',
            route('web.buy'),
            asset('frontend/assets/images/share.png')
        );
        $filter = new FilterController();
        $filter->clearAllData();

        $properties = Property::sale()->available()->get();


        return view('web.filter', [
            'head' => $head,
            'properties' => $properties,
            'type' => 'sale'
        ]);
    }

    public function buyProperty(Request $request)
    {

        $property = Property::where('slug', $request->slug)->first();
        $property->views = $property->views + 1;
        $property->save();

        return view('web.property', [
            'property' => $property,
            'type' => 'sale'
        ]);
    }

    public function rentProperty(Request $request)
    {

        $property = Property::where('slug', $request->slug)->first();

        $property->views = $property->views + 1;
        $property->save();

        return view('web.property', [
            'property' => $property,
            'type' => 'rent'
        ]);
    }

    public function filter()
    {
        $head = $this->seo->render(
            env('APP_NAME') . ' Upinside treinamentos',
            'procure o imoveis do seu sonho',
            route('web.buy'),
            asset('frontend/assets/images/share.png')
        );

        $filter = new FilterController();
        $itemProperties = $filter->createQuery('id');

        foreach ($itemProperties as $property) {
            $properties[] = $property->id;
        }


        if (!empty($properties)) {
            $properties = Property::whereIn('id', $properties)->get();
        } else {
            $properties = Property::all();
        }


        return view('web.filter', [
            'head' => $head,
            'properties' => $properties
        ]);
    }

    public function contact()
    {
        return view('web.contact');
    }

    public function sendEmail(Request $request)
    {



        $data = [
            'reply_name' => $request->name,
            'reply_email' => $request->email,
            'cell' => $request->cell,
            'message' => $request->message
        ];

        Mail::send(new Contact($data));

        return  redirect()->route('web.sendEmailSuccess');
    }


    public function sendEmailSuccess()
    {
        return view('web.contact_success');
    }

    public function expirence()
    {
        $head = $this->seo->render(
            env('APP_NAME') . ' Upinside treinamentos',
            'experiencias o imoveis do seu sonho',
            route('web.buy'),
            asset('frontend/assets/images/share.png')
        );
        $filter = new FilterController();
        $filter->clearAllData();

        $properties = Property::whereNotNull('experience')->get();


        return view('web.filter', [
            'properties' => $properties
        ]);
    }


    public function expirenceCategory(Request $request)
    {
        $filter = new FilterController();
        $filter->clearAllData();

        if ($request->slug == 'cobertura') {
            $properties = Property::where('experience', 'Cobertura')->get();
            return view('web.filter', [
                'properties' => $properties
            ]);
        } else if ($request->slug == 'alto-padrao') {
            $properties = Property::where('experience', 'Alto Padrão')->get();
            return view('web.filter', [
                'properties' => $properties
            ]);
        } else if ($request->slug == 'de-frente-para-o-mar') {
            $properties = Property::where('experience', 'De Frente para o Mar')->get();
            return view('web.filter', [
                'properties' => $properties
            ]);
        } else if ($request->slug == 'condominio-fechado') {
            $properties = Property::where('experience', 'Condomínio Fechado')->get();
            return view('web.filter', [
                'properties' => $properties
            ]);
        } else if ($request->slug == 'compacto') {
            $properties = Property::where('experience', 'Compacto')->get();
            return view('web.filter', [
                'properties' => $properties
            ]);
        } else if ($request->slug == 'lojas-e-salas') {
            $properties = Property::where('experience', 'Lojas e Salas')->get();
            return view('web.filter', [
                'properties' => $properties
            ]);
        } else {
            $properties = Property::whereNotNull('experience')->get()->all();
        }
        return view('web.filter', [
            'properties' => $properties
        ]);
    }
}

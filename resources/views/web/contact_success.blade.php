@extends('web.master.master')

@section('content')

<div class="container p-5"> 

    <h2 class="  text-center text-front bg-white">Seu email foi enviado com sucesso! Em breve retonaremos</h2>
    <p class="text-center">   <a href="{{url()->previous()}}">Continuar navegando</a></p>
    
</div>


@endsection
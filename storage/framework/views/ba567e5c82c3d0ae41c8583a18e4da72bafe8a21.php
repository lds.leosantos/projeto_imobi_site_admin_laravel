<?php $__env->startSection('content'); ?>
    <div class="main_filter bg-light py-5">
        <div class="container">
            <section class="row">
                <div class="col-12">
                    <h2 class="text-front icon-filter mb-5">Filtro</h2>
                </div>

                <div class="col-12 col-md-4">
                    <form action=" <?php echo e(route('web.filter')); ?>" method="post" class="w-100 p-3 bg-white mb-5">
                        <?php echo csrf_field(); ?>
                        <div class="row">
                            <div class="form-group col-12">
                                <label for="search" class="mb-2 text-front">Comprar ou Alugar?</label>
                                <select class="selectpicker" id="search" name="filter_search" title="Escolha..."
                                    data-index="1" data-action="<?php echo e(route('component.main-filter.search')); ?>">
                                    <option value="buy">Comprar</option>
                                    <option value="rent">Alugar</option>
                                </select>
                            </div>

                            <div class="form-group col-12">
                                <label for="category" class="mb-2 text-front">O que você quer?</label>
                                <select class="selectpicker" id="category" name="filter_category" title="Escolha..."
                                    data-index="2" data-action="<?php echo e(route('component.main-filter.category')); ?>">
                                    <option disabled>Selecione o Filtro anterior</option>
                                </select>
                            </div>

                            <div class="form-group col-12">
                                <label for="type" class="mb-2 text-front">Qual o tipo do imóvel?</label>
                                <select class="selectpicker input-large" id="type" name="filter_type" title="Escolha..."
                                    multiple data-actions-box="true" data-index="3"
                                    data-action="<?php echo e(route('component.main-filter.type')); ?>">
                                    <option disabled>Selecione o Filtro anterior</option>
                                </select>
                            </div>

                            <div class="form-group col-12">
                                <label for="search_locale" class="mb-2 text-front">Onde você quer?</label>
                                <select class="selectpicker" name="filter_neighborhood" id="bedrooms" title="Escolha..."
                                    multiple data-actions-box="true" data-index="4"
                                    data-action="<?php echo e(route('component.main-filter.neighborhood')); ?>">
                                    <option disabled>Selecione o Filtro anterior</option>
                                </select>
                            </div>

                            <div class="form-group col-12">
                                <label for="bedrooms" class="mb-2 text-front">Quartos</label>
                                <select class="selectpicker" name="filter_bedrooms" id="bedrooms" title="Escolha..."
                                    data-index="5" data-action="<?php echo e(route('component.main-filter.bedrooms')); ?>">
                                    <option disabled>Selecione o Filtro anterior</option>

                                </select>
                            </div>

                            <div class="form-group col-12">
                                <label for="bedrooms" class="mb-2 text-front">Suítes</label>
                                <select class="selectpicker" name="filter_suites" id="bedrooms" title="Escolha..."
                                    data-index="6" data-action="<?php echo e(route('component.main-filter.suites')); ?>">
                                    <option disabled>Selecione o Filtro anterior</option>

                                </select>
                            </div>

                            <div class="form-group col-12">
                                <label for="bedrooms" class="mb-2 text-front">Banheiros</label>
                                <select class="selectpicker" name="filter_bathrooms" id="bedrooms" title="Escolha..."
                                    data-index="7" data-action="<?php echo e(route('component.main-filter.bathrooms')); ?>">
                                    <option disabled>Selecione o Filtro anterior</option>

                                </select>
                            </div>

                            <div class="form-group col-12">
                                <label for="bedrooms" class="mb-2 text-front">Garagem</label>
                                <select class="selectpicker" name="filter_garage" id="bedrooms" title="Escolha..."
                                    data-index="8" data-action="<?php echo e(route('component.main-filter.garage')); ?>">
                                    <option disabled>Selecione o Filtro anterior</option>

                                </select>
                            </div>

                            <div class="form-group col-12">
                                <label for="bedrooms" class="mb-2 text-front">Preço Base</label>
                                <select class="selectpicker" name="filter_base" id="bedrooms" title="Escolha..."
                                    data-index="9" data-action="<?php echo e(route('component.main-filter.priceBase')); ?>">
                                    <option disabled>Selecione o Filtro anterior</option>

                                </select>
                            </div>

                            <div class="form-group col-12">
                                <label for="bedrooms" class="mb-2 text-front">Preço Limite</label>
                                <select class="selectpicker" name="filter_limit" id="bedrooms" title="Escolha..."
                                    data-index="10" data-action="<?php echo e(route('component.main-filter.priceLimit')); ?>">
                                    <option disabled>Selecione o Filtro anterior</option>

                                </select>
                            </div>

                            <div class="col-12 text-right mt-3 button_search">
                                <button class="btn btn-front icon-search">Pesquisar</button>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="col-12 col-md-8">

                    <section class="row main_properties">
                        <?php if($properties->count()): ?>
                            <?php $__currentLoopData = $properties; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $property): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="col-12 col-md-12 col-lg-6 mb-4">
                                    <article class="card main_properties_item">
                                        <div class="img-responsive-16by9">
                                            <a
                                                href="<?php echo e(route((session('sale') == true || (!empty($type) && $type == 'sale') || $property->rent == false ? 'web.buyProperty' : 'web.rentProperty'), ['property' => $property->slug])); ?>">
                                                <img src="<?php echo e($property->cover()); ?>" class="card-img-top" alt="">
                                            </a>
                                        </div>
                                        <div class="card-body">
                                            <h2><a href="<?php echo e(route((session('sale') == true || (!empty($type) && $type == 'sale') || $property->rent == false ? 'web.buyProperty' : 'web.rentProperty'), ['property' => $property->slug])); ?>"
                                                    class="text-front"><?php echo e($property->title); ?></a>
                                            </h2>
                                            <p class="main_properties_item_category"><?php echo e($property->category); ?></p>
                                            <p class="main_properties_item_type"><?php echo e($property->type); ?> -
                                                <?php echo e($property->neighborhood); ?> <i
                                                    class="icon-location-arrow icon-notext"></i></p>
                                            <?php if(!empty($type) && $type == 'sale'): ?>
                                                <p class="main_properties_price text-front">R$
                                                    <?php echo e($property->sale_price); ?></p>
                                                <?php elseif(!empty($type) && $type =='rent'): ?>
                                                    <p class="main_properties_price text-front">R$
                                                        <?php echo e($property->rent_price); ?> /mês
                                                    </p>
                                                <?php else: ?>
                                                <?php if($property->sale == true && !empty($property->sale_price) && $property->rent == true && !empty($property->rent_price)): ?>
                                                    <p class="main_properties_price text-front">R$
                                                        <?php echo e($property->sale_price); ?> <br> ou
                                                        <?php echo e($property->rent_price); ?> /mês </p>
                                                <?php elseif($property->sale == true && !empty($property->sale_price)): ?>
                                                    <p class="main_properties_price text-front">R$
                                                        <?php echo e($property->sale_price); ?></p>
                                                <?php elseif($property->rent == true && !empty($property->rent_price)): ?>
                                                    <p class="main_properties_price text-front">R$
                                                        <?php echo e($property->rent_price); ?> / mês</p>
                                                <?php else: ?>
                                                    <p class="main_properties_price text-front"> Entre em contato com a
                                                        nossa equipe comercial!</p>
                                                <?php endif; ?>
                                            <?php endif; ?>

                                            <a href="<?php echo e(route((session('sale') == true || (!empty($type) && $type == 'sale') || $property->rent == false ? 'web.buyProperty' : 'web.rentProperty'), ['property' => $property->slug])); ?>"
                                                class="btn btn-front btn-block">Ver Imóvel</a>
                                        </div>
                                        <div class="card-footer d-flex">
                                            <div class="main_properties_features col-4 text-center">
                                                <img src="<?php echo e(asset('frontend/assets/images/icons/bed.png')); ?>" class="img-fluid"
                                                    alt="">
                                                <p class="text-muted"><?php echo e($property->bathrooms); ?></p>
                                            </div>
                                            <div class="main_properties_features col-4 text-center">
                                                <img src="<?php echo e(asset('frontend/assets/images/icons/garage.png')); ?>" class="img-fluid"
                                                    alt="">
                                                <p class="text-muted">
                                                    <?php echo e($property->garage + $property->garage_covered); ?></p>
                                            </div>
                                            <div class="main_properties_features col-4 text-center">
                                                <img src="<?php echo e(asset('frontend/assets/images/icons/util-area.png')); ?>" class="img-fluid"
                                                    alt="">
                                                <p class="text-muted"><?php echo e($property->area_util); ?>&sup2;</p>
                                            </div>
                                        </div>
                                    </article>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php else: ?>
                            <div class="col-12 p-5 bg-white">
                                    <h2 class="text-front icon-info text-center">Nenhum registro encontrado!</h2>
                                    <p class="text-center">Utilize o filtro alvançado para encontar o imovel adequado</p>
                            </div>
                        <?php endif; ?>
                    </section>
                </div>
            </section>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('web.master.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
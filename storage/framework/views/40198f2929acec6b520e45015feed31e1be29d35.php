<?php $__env->startSection('content'); ?>

<div class="container p-5"> 

    <h2 class="  text-center text-front bg-white">Seu email foi enviado com sucesso! Em breve retonaremos</h2>
    <p class="text-center">   <a href="<?php echo e(url()->previous()); ?>">Continuar navegando</a></p>
    
</div>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('web.master.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startSection('content'); ?>
    <section class="dash_content_app">

        <header class="dash_content_app_header">
            <h2 class="icon-search">Filtro</h2>

            <div class="dash_content_app_header_actions">
                <nav class="dash_content_app_breadcrumb">
                    <ul>
                        <li><a href="<?php echo e(route('admin.home')); ?>">Dashboard</a></li>
                        <li class="separator icon-angle-right icon-notext"></li>
                        <li><a href="<?php echo e(route('admin.users.index')); ?>" class="text-orange">Clientes</a></li>
                    </ul>
                </nav>

                <a href="<?php echo e(route('admin.users.create')); ?>" class="btn btn-orange icon-user ml-1">Criar Cliente</a>
                <button class="btn btn-green icon-search icon-notext ml-1 search_open"></button>
            </div>
        </header>

        <?php echo $__env->make('admin.users.filter', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>;

        <div class="dash_content_app_box">
            <div class="dash_content_app_box_stage">
                <table id="dataTable" class="nowrap stripe" width="100" style="width: 100% !important;">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nome Completo</th>
                            <th>CPF</th>
                            <th>E-mail</th>
                            <th>Nascimento</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td><?php echo e($user->id); ?></td>
                                <td><a href="<?php echo e(route('admin.users.edit',['user'=>$user->id])); ?>" class="text-orange"><?php echo e($user->name); ?></a></td>
                                <td><?php echo e($user->document); ?></td>
                                <td><a href="mailto:<?php echo e($user->email); ?>" class="text-orange"><?php echo e($user->email); ?></a></td>
                                <td><?php echo e($user->date_of_birth); ?></td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.master.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Apache24\htdocs\project\resources\views/admin/users/index.blade.php ENDPATH**/ ?>
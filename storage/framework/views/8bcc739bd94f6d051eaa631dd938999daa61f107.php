;

<?php $__env->startSection('content'); ?>


    <section class="dash_content_app">

        <header class="dash_content_app_header">
            <h2 class="icon-search">Filtro</h2>

            <div class="dash_content_app_header_actions">
                <nav class="dash_content_app_breadcrumb">
                    <ul>
                        <li><a href="<?php echo e(route('admin.home')); ?>">Dashboard</a></li>
                        <li class="separator icon-angle-right icon-notext"></li>
                        <li><a href="<?php echo e(route('admin.users.index')); ?>">Clientes</a></li>
                        <li class="separator icon-angle-right icon-notext"></li>
                        <li><a href="<?php echo e(route('admin.companies.index')); ?>}" class="text-orange">Empresas</a></li>
                        
                    </ul>
                </nav>

                <a href="<?php echo e(route('admin.companies.create')); ?>" class="btn btn-orange icon-building-o ml-1">Criar Empresa</a>
                <button class="btn btn-green icon-search icon-notext ml-1 search_open"></button>
            </div>
        </header>

        <?php echo $__env->make('admin.companies.filter', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <div class="dash_content_app_box">
            <div class="dash_content_app_box_stage">
                <table id="dataTable" class="nowrap hover stripe" width="100" style="width: 100% !important;">
                    <thead>
                        <tr>
                            <th>Razão Social</th>
                            <th>Nome Fantasia</th>
                            <th>CNPJ</th>
                            <th>IE</th>
                            <th>Responsável</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $__currentLoopData = $companies; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $company): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            
                        <tr>
                            <td><a href="<?php echo e(route('admin.companies.edit',['company'=>$company->id])); ?>" class="text-orange"><?php echo e($company->social_name); ?></a></td>
                            <td><?php echo e($company->alias_name); ?></td>
                            <td><?php echo e($company->document_company); ?></td>
                            <td><?php echo e($company->document_company_secondary); ?></td>
                            <td><a href="<?php echo e(route('admin.users.edit',['user'=>$company->owner->id])); ?>" class="text-orange"><?php echo e($company->owner->name); ?></a></td>
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.master.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Apache24\htdocs\project\resources\views/admin/companies/index.blade.php ENDPATH**/ ?>
<?php $__env->startSection('content'); ?>

    <section class="dash_content_app">

        <header class="dash_content_app_header">
            <h2 class="icon-user-plus">Nova Empresa</h2>

            <div class="dash_content_app_header_actions">
                <nav class="dash_content_app_breadcrumb">
                    <ul>
                        <li><a href="<?php echo e(route('admin.home')); ?>">Dashboard</a></li>
                        <li class="separator icon-angle-right icon-notext"></li>
                        <li><a href="<?php echo e(route('admin.users.index')); ?>">Clientes</a></li>
                        <li class="separator icon-angle-right icon-notext"></li>
                        <li><a href="<?php echo e(route('admin.companies.index')); ?>">Empresas</a></li>
                    </ul>
                </nav>
            </div>
        </header>

        <?php if($errors->all()): ?>
            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php $__env->startComponent('admin.components.message', ['color'=>'orange']); ?>
                <p class="icon-asterisk"> <?php echo e($error); ?></p>
                <?php echo $__env->renderComponent(); ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php endif; ?>
        <div class="dash_content_app_box">
            <div class="dash_content_app_box_stage">
                <form class="app_form" action="<?php echo e(route('admin.companies.store')); ?>" method="post">
                    <?php echo csrf_field(); ?>
                    <label class="label">
                        <span class="legend">Responsável Legal:</span>
                        <select name="user" class="select2">
                            <option value="" selected>Selecione um responsável</option>
                            <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if(!empty($selected) ): ?>
                                <option value="<?php echo e($user->id); ?>" <?php echo e($user->id  === $selected->id ? 'selected' : ''); ?> > <?php echo e($user->name); ?> - (<?php echo e($user->document); ?>)</option>
                                <?php else: ?> 
                                <option value="<?php echo e($user->id); ?>" > <?php echo e($user->name); ?> - (<?php echo e($user->document); ?>)</option>

                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                        <?php if(!empty($selected->id)): ?>    
                        <p style="margin-top: 4px;">
                            <a href="" class="text-orange icon-link" style="font-size: .8em;" target="_blank">Acessar Cadastro</a>
                        </p>
                        <?php endif; ?>
                    </label>
                    <label class="label">
                        <span class="legend">*Razão Social:</span>
                        <input type="text" name="social_name" placeholder="Razão Social" value="<?php echo e(old('social_name')); ?>" />
                    </label>

                    <label class="label">
                        <span class="legend">Nome Fantasia:</span>
                        <input type="text" name="alias_name" placeholder="Nome Fantasia" value="<?php echo e(old('alias_name')); ?>"  />
                    </label>

                    <div class="label_g2">
                        <label class="label">
                            <span class="legend">CNPJ:</span>
                            <input type="text" name="document_company" class="mask-cnpj" placeholder="CNPJ da Empresa"
                            value="<?php echo e(old('document_company')); ?>" />
                        </label>

                        <label class="label">
                            <span class="legend">Inscrição Estadual:</span>
                            <input type="text" name="document_company_secondary" placeholder="Número da Inscrição"
                            value="<?php echo e(old('document_company_secondary')); ?>" />
                        </label>
                    </div>

                    <div class="app_collapse">
                        <div class="app_collapse_header mt-2 collapse">
                            <h3>Endereço</h3>
                            <span class="icon-minus-circle icon-notext"></span>
                        </div>

                        <div class="app_collapse_content">
                            <div class="label_g2">
                                <label class="label">
                                    <span class="legend">*CEP:</span>
                                    <input type="text" name="zipcode" class="mask-zipcode zip_code_search"
                                        placeholder="Digite o CEP"  value="<?php echo e(old('zipcode')); ?>" />
                                </label>
                            </div>

                            <label class="label">
                                <span class="legend">*Endereço:</span>
                                <input type="text" name="street" class="street" placeholder="Endereço Completo"
                                value="<?php echo e(old('street')); ?>" />
                            </label>

                            <div class="label_g2">
                                <label class="label">
                                    <span class="legend">*Número:</span>
                                    <input type="text" name="number" placeholder="Número do Endereço" value="<?php echo e(old('number')); ?>" />
                                </label>

                                <label class="label">
                                    <span class="legend">Complemento:</span>
                                    <input type="text" name="complement" placeholder="Completo (Opcional)" value="<?php echo e(old('complement')); ?>" />
                                </label>
                            </div>

                            <label class="label">
                                <span class="legend">*Bairro:</span>
                                <input type="text" name="neighborhood" class="neighborhood" placeholder="Bairro"
                                value="<?php echo e(old('neighborhood')); ?>" />
                            </label>

                            <div class="label_g2">
                                <label class="label">
                                    <span class="legend">*Estado:</span>
                                    <input type="text" name="state" class="state" placeholder="Estado" value="<?php echo e(old('state')); ?>" />
                                </label>

                                <label class="label">
                                    <span class="legend">*Cidade:</span>
                                    <input type="text" name="city" class="city" placeholder="Cidade" value="<?php echo e(old('city')); ?>" />
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="text-right">
                        <button class="btn btn-large btn-green icon-check-square-o" type="submit">Criar Empresa</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.master.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startSection('content'); ?>

    <section class="main_property">
        <div class="main_property_header py-5 bg-light">
            <div class="container">
                <h1 class="text-front"><?php echo e($property->title); ?></h1>
                <p class="mb-0"><?php echo e($property->category); ?> - <?php echo e($property->type); ?> -
                    <?php echo e($property->neighborhood); ?>

                </p>
            </div>
        </div>
        <div class="main_property_content py-5">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-lg-8">
                        <div id="carouselProperty" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <?php if($property->images()->get()->count()): ?>
                                    <?php $__currentLoopData = $property->images()->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li data-target="#carouselProperty" data-slide-to="<?php echo e($loop->iteration); ?>"
                                            <?php echo $loop->iteration == 1 ? 'class="active"' : ''; ?>></li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                            </ol>

                            <div class="carousel-inner">

                                <?php if($property->images()->get()->count()): ?>
                                    <?php $__currentLoopData = $property->images()->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="carousel-item  <?php echo e($loop->iteration == 1 ? 'active' : ''); ?> ">
                                            <a href="<?php echo e($image->getUrlCroppedAttribute()); ?>" data-toggle="lightbox"
                                                data-gallery="property-gallery" data-type="image">
                                                <img src="<?php echo e($image->getUrlCroppedAttribute()); ?>" class="d-block w-100"
                                                    alt="<?php echo e($property->title); ?>">
                                            </a>
                                        </div>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                            </div>
                            <a class="carousel-control-prev" href="#carouselProperty" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Anterior</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselProperty" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Próximo</span>
                            </a>
                        </div>

                        <div class="main_property_price pt-4 text-muted">
                            <p class="main_property_price_small">IPTU: R$ <?php echo e($property->tribute); ?>

                                <?php echo e($property->condominium != '0.00' ? ' | Condomínio: R$' . $property->condominium : ''); ?>

                            </p>

                            <?php if(!empty($type) && $type == 'sale'): ?>
                            <p class="main_property_price_big">Valor do Imovel: R$ <?php echo e($property->sale_price); ?></p>

                            <?php elseif(!empty($type) && $type =='rent'): ?>
                            <p class="main_property_price_big">Valor do Aluguel: R$ <?php echo e($property->rent_price); ?></p>

                            <?php else: ?>
                            <?php if($property->sale == true && !empty($property->sale_price) && $property->rent == true && !empty($property->rent_price)): ?>
                            <p class="main_property_price_big">Valor do Imovel: R$ <?php echo e($property->sale_price); ?> <br> ou 
                                Valor do Aluguel: R$ <?php echo e($property->rent_price); ?>

                            </p>

                            <?php elseif($property->sale == true && !empty($property->sale_price)): ?>
                            <p class="main_property_price_big">Valor do Imovel: R$ <?php echo e($property->sale_price); ?></p>

                            <?php elseif($property->rent == true && !empty($property->rent_price)): ?>
                            <p class="main_property_price_big">Valor do Aluguel: R$ <?php echo e($property->rent_price); ?></p>

                            <?php else: ?>
                                <p class="main_properties_price text-front"> Entre em contato com a
                                    nossa equipe comercial!</p>
                            <?php endif; ?>
                        <?php endif; ?>
                        </div>

                        <div class="main_property_content_description">
                            <h2 class="text-front">Conheça mais o imóvel</h2>
                            <?php echo $property->description; ?>

                            </p>
                            </p>
                        </div>

                        <div class="main_property_content_features">
                            <h2 class="text-front">Características</h2>
                            <table class="table table-striped" style="margin-bottom: 40px;">
                                <tbody>
                                    <tr>
                                        <td>Domitórios</td>
                                        <td><?php echo e($property->bedrooms); ?></td>
                                    </tr>
                                    <tr>
                                        <td>Suítes</td>
                                        <td><?php echo e($property->suites); ?></td>
                                    </tr>
                                    <tr>
                                        <td>Banheiros</td>
                                        <td><?php echo e($property->bathrooms); ?></td>
                                    </tr>
                                    <tr>
                                        <td>Salas</td>
                                        <td><?php echo e($property->rooms); ?></td>
                                    </tr>
                                    <tr>
                                        <td>Garagem</td>
                                        <td><?php echo e($property->garage); ?></td>
                                    </tr>
                                    <tr>
                                        <td>Garagem Coberta</td>
                                        <td><?php echo e($property->garage_covered); ?></td>
                                    </tr>
                                    <tr>
                                        <td>Área Total</td>
                                        <td><?php echo e($property->area_total); ?> m&sup2;</td>
                                    </tr>
                                    <tr>
                                        <td>Área Útil</td>
                                        <td><?php echo e($property->area_util); ?> m&sup2;</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="main_property_structure">
                            <h2 class="text-front">Estrutura</h2>

                            <div class="row">
                                <?php if($property->air_conditioning == true): ?>
                                    <span class="main_property_structure_item icon-check">Ar Condicionado</span>
                                <?php endif; ?>

                                <?php if($property->bar == true): ?>
                                    <span class="main_property_structure_item icon-check">Bar</span>
                                <?php endif; ?>

                                <?php if($property->library == true): ?>
                                    <span class="main_property_structure_item icon-check">Biblioteca</span>
                                <?php endif; ?>

                                <?php if($property->barbecue_grill == true): ?>
                                    <span class="main_property_structure_item icon-check">Churrasqueira</span>
                                <?php endif; ?>

                                <?php if($property->american_kitchen == true): ?>
                                    <span class="main_property_structure_item icon-check">Cozinha Americana</span>
                                <?php endif; ?>

                                <?php if($property->fitted_kitchen == true): ?>
                                    <span class="main_property_structure_item icon-check">Cozinha Planejada</span>
                                <?php endif; ?>

                                <?php if($property->pantry == true): ?>
                                    <span class="main_property_structure_item icon-check">Despensa</span>
                                <?php endif; ?>

                                <?php if($property->edicule == true): ?>
                                    <span class="main_property_structure_item icon-check">Edicula</span>
                                <?php endif; ?>

                                <?php if($property->office == true): ?>
                                    <span class="main_property_structure_item icon-check">Escritório</span>
                                <?php endif; ?>

                                <?php if($property->bathtub == true): ?>
                                    <span class="main_property_structure_item icon-check">Banheira</span>
                                <?php endif; ?>

                                <?php if($property->fireplace == true): ?>
                                    <span class="main_property_structure_item icon-check">Lareira</span>
                                <?php endif; ?>

                                <?php if($property->lavatory == true): ?>
                                    <span class="main_property_structure_item icon-check">Lavabo</span>
                                <?php endif; ?>

                                <?php if($property->furnished == true): ?>
                                    <span class="main_property_structure_item icon-check">Mobiliado</span>
                                <?php endif; ?>

                                <?php if($property->pool == true): ?>
                                    <span class="main_property_structure_item icon-check">Piscina</span>
                                <?php endif; ?>

                                <?php if($property->steam_room == true): ?>
                                    <span class="main_property_structure_item icon-check">Sauna</span>
                                <?php endif; ?>

                                <?php if($property->view_of_the_sea == true): ?>
                                    <span class="main_property_structure_item icon-check">Vista para o Mar</span>
                                <?php endif; ?>

                            </div>
                        </div>

                        <div class="main_property_location">
                            <h2 class="text-front">Localização</h2>
                            <div id="map" style="width: 100%; min-height: 400px;"></div>
                        </div>

                    </div>
                    <div class="col-12 col-lg-4">
                        <a href="https://api.whatsapp.com/send?phone=DDI+DDD+TELEFONE&text=Olá, preciso de ajuda com o login."  class="btn btn-outline-success btn-lg btn-block icon-whatsapp mb-3">Converse com o Corretor!
                        </a>

                        <div class="main_property_contact">
                            <h2 class="bg-front text-white">Entre em contato</h2>

                            <form action="<?php echo e(route('web.sendEmail')); ?>" method="post" autocomplete="off">
                                <?php echo csrf_field(); ?>
                                <div class="form-group">
                                    <label for="name">Seu nome:</label>
                                    <input type="text"  class="form-control" name="name"
                                        placeholder="Informe seu nome completo">
                                </div>

                                <div class="form-group">
                                    <label for="telephone">Seu telefone:</label>
                                    <input type="tel" name="cell" class="form-control"
                                        placeholder="Informe seu telefone com DDD">
                                </div>

                                <div class="form-group">
                                    <label for="email">Seu e-mail:</label>
                                    <input type="email" name="email" class="form-control"
                                        placeholder="Informe seu melhor e-mail">
                                </div>

                                <div class="form-group">
                                    <label for="message">Sua Mensagem:</label>
                                    <textarea name="message" id="message" cols="30" rows="5"
                                        class="form-control">Quero ter mais informações sobre esse imóvel. Imóvel Residencial, Casa, Campeche, Florianópolis! (#01)</textarea>
                                </div>

                                <div class="form-group">
                                    <button class="btn btn-block btn-front">Enviar</button>
                                    <p class="text-center text-front mb-0 mt-4 font-weight-bold">(48) 3322-1234</p>
                                </div>
                            </form>
                        </div>

                        <div class="main_property_share py-3 text-right">
                            <span class="text-front mr-2">Compartilhe:</span>
                            <button class="btn btn-front icon-facebook icon-notext"></button>
                            <button class="btn btn-front icon-twitter icon-notext"></button>
                            <button class="btn btn-front icon-instagram icon-notext"></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
    <script>
        function markMap() {

            var locationJson = $.getJSON(
                'https://maps.googleapis.com/maps/api/geocode/json?address=Rodovia+Doutor+Antonio+Luiz+Moura+Gonzaga,+3339+Florianopolis+Campeche&key=',
                function(response) {
                    console.log(response.results[0].geometry.location.lat);
                    console.log(response.results[0].geometry.location.lng);

                    lat = response.results[0].geometry.location.lat;
                    lng = response.results[0].geometry.location.lng;

                    var citymap = {
                        property: {
                            center: {
                                lat: lat,
                                lng: lng
                            },
                            population: 100
                        }
                    };

                    var map = new google.maps.Map(document.getElementById('map'), {
                        zoom: 14,
                        center: {
                            lat: lat,
                            lng: lng
                        },
                        mapTypeId: 'terrain'
                    });

                    for (var city in citymap) {
                        var cityCircle = new google.maps.Circle({
                            strokeColor: '#FF0000',
                            strokeOpacity: 0.8,
                            strokeWeight: 2,
                            fillColor: '#FF0000',
                            fillOpacity: 0.35,
                            map: map,
                            center: citymap[city].center,
                            radius: Math.sqrt(citymap[city].population) * 100
                        });
                    }
                });
        }
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCI8i3U8g5vCZc3GgRblC_cmCX1JdZZP4o&callback=markMap"></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('web.master.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Apache24\htdocs\project\resources\views/web/property.blade.php ENDPATH**/ ?>
<?php


use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'Web', 'as' => 'web.'], function() {
    
    /**pagina inicial do site */
    Route::get('/' ,'WebController@home')->name('home');
    /** pagina destaque */
        Route::get('/destaque', 'WebController@spotlight')->name('spotlight');

     /**pagina logação do site */
     Route::get('/quero-alugar' ,'WebController@rent')->name('rent');

      /**pagina comprar do site */
    Route::get('/quero-comprar' ,'WebController@buy')->name('buy');

        /**pagina comprar um imovel  do site */
        Route::get('/quero-comprar/{slug}' ,'WebController@buyProperty')->name('buyProperty');

         /**pagina alugar um imoveil do site */
         Route::get('/quero-alugar/{slug}' ,'WebController@rentProperty')->name('rentProperty');

     /**pagina filtro do site */
     Route::match(['post', 'get'],'/filtro' ,'WebController@filter')->name('filter');

     /**paginas de experiencias */
     Route::get('/experiencias','WebController@expirence')->name('expirence');

    /**paginas de experiencias - especifica de uma categoria */
     Route::get('/experiencias/{slug}','WebController@expirenceCategory')->name('expirenceCategory');


     /**pagina contato do site */
    Route::get('/contato' ,'WebController@contact')->name('contact');
    Route::post('/contato/sendEmail' ,'WebController@sendEmail')->name('sendEmail');
    Route::get('/contato/sucesso' ,'WebController@sendEmailSuccess')->name('sendEmailSuccess');



});

Route::group(['prefix'=>'component','namespace'=>'Web', 'as' => 'component.'], function(){
        Route::post('main-filter/search', 'FilterController@search')->name('main-filter.search');
        Route::post('main-filter/category','FilterController@category')->name('main-filter.category');
        Route::post('main-filter/type','FilterController@type')->name('main-filter.type');
        Route::post('main-filter/neighborhood','FilterController@neighborhood')->name('main-filter.neighborhood');
        Route::post('main-filter/bedrooms','FilterController@bedrooms')->name('main-filter.bedrooms');
        Route::post('main-filter/suites','FilterController@suites')->name('main-filter.suites');
        Route::post('main-filter/bathrooms','FilterController@bathrooms')->name('main-filter.bathrooms');
        Route::post('main-filter/garage','FilterController@garage')->name('main-filter.garage');
        Route::post('main-filter/price-base','FilterController@priceBase')->name('main-filter.priceBase');
        Route::post('main-filter/price-limit','FilterController@priceLimit')->name('main-filter.priceLimit');



});


Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'as' => 'admin.'], function () {

    /** formulário de login */
    Route::get('/', 'AuthController@showLoginForm')->name('login');
    Route::post('login', 'AuthController@login')->name('login.do');

    /** rotas protegidas */
    Route::group(['middleware' => ['auth']], function () {
        /**dashboard home*/
        Route::get('home', 'AuthController@home')->name('home');
        Route::get('users/team', 'UserController@team')->name('users.team');

        /**users */
        Route::resource('users', 'UserController');

        /**empresas*/
        Route::resource('companies', 'CompanyController');

        /**imoves*/
        Route::post('properties/image-set-cover', 'PropertyController@imageSetCover')->name('properties.imageSetCover');
        Route::delete('properties/image-remove', 'PropertyController@imageRemove')->name('properties.imageRemove');
        Route::resource('properties',   'PropertyController');

        /**Contratos */
        Route::post('contracts/get-data-owner','ContractController@getDataOwner')->name('contracts.getDataOwner');
        Route::post('contracts/get-data-acquirer','ContractController@getDataAcquirer')->name('contracts.getDataAcquirer');
        Route::post('contracts/get-data-property','ContractController@getDataProperty')->name('contracts.getDataProperty');

        Route::resource('contracts', 'ContractController');

    });

    /** Logout*/
    Route::get('logout', 'AuthController@logout')->name('logout');
});
